#version 120

uniform int active_lights_n; // Number of active lights (< MG_MAX_LIGHT)
uniform vec3 scene_ambient; // Scene ambient light

struct material_t {
	vec3  diffuse;
	vec3  specular;
	float alpha;
	float shininess;
};

struct light_t {
	vec4 position;    // Camera space
	vec3 diffuse;     // rgb
	vec3 specular;    // rgb
	vec3 attenuation; // (constant, lineal, quadratic)
	vec3 spotDir;     // Camera space
	float cosCutOff;  // cutOff cosine
	float exponent;
};

uniform light_t theLights[4];
uniform material_t theMaterial;

uniform sampler2D texture0;
uniform sampler2D bumpmap;

varying vec2 f_texCoord;
varying vec3 f_viewDirection;     // tangent space
varying vec3 f_lightDirection[4]; // tangent space
varying vec3 f_spotDirection[4];  // tangent space

////////////////////////////////////


vec4 argiInfinitua(int i){

	vec4 pos, normal, r;
	vec3 Idiff, Ispec;

	normal = normalize(vec4((texture2D(bumpmap, f_texCoord).rgb * 2.0 - 1.0), 0.0));

	vec4 l = normalize(vec4(f_lightDirection[i], 0.0));

	Idiff = theLights[i].diffuse * theMaterial.diffuse;

	r = ( 2*(dot(normal, l)) * normal ) - l;

	Ispec = pow( max(0.0, dot(r, normalize(vec4(f_viewDirection, 0.0))  )  ) , theMaterial.shininess) * (theMaterial.specular * theLights[i].specular);
	
	return ( max(0.0, dot(normal, l)) * vec4(Idiff + Ispec, 0.0));

}


vec4 argiLokala(int i){

	
	
	float sc = theLights[i].attenuation[0];
	float sl = theLights[i].attenuation[1];
	float sq = theLights[i].attenuation[2];

	float d = 1/( sc + sl*length(f_lightDirection[i]) + sq * pow(length(f_lightDirection[i]), 2) );

	return d *  argiInfinitua(i);
}

vec4 argiSpotlight(int i){

	vec4 l = normalize(vec4(f_lightDirection[i], 0.0));

	float cSpot = max( dot(-1 *l , vec4(normalize(f_spotDirection[i]), 0)) , 0);

	if(cSpot < theLights[i].cosCutOff){
		return vec4(0);
	}

	return pow(cSpot, theLights[i].exponent) * argiLokala(i); 

}

/////////////////////////////77

void main() {
	
	vec4 baseColor = texture2D(texture0, f_texCoord);

	//vec3 N = texture2D(bumpmap, f_texCoord).rgb * 2.0 - 1.0;

	
	vec4 argia, f_color;

	f_color = vec4(scene_ambient, 1);


	for(int i=0; i<active_lights_n; i++){
		if(theLights[i].position.w == 0){
			argia = argiInfinitua(i);
		}else{
			if(theLights[i].cosCutOff==0){
				argia = argiLokala(i);
			}else{
				argia = argiSpotlight(i);
			}
		}

		f_color += argia;
	}
	

	gl_FragColor = f_color * baseColor;
}



