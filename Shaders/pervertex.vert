#version 120

uniform mat4 modelToCameraMatrix;
uniform mat4 cameraToClipMatrix;
uniform mat4 modelToWorldMatrix;
uniform mat4 modelToClipMatrix;

uniform int active_lights_n; // Number of active lights (< MG_MAX_LIGHT)
uniform vec3 scene_ambient;  // rgb / Iamb

uniform struct light_t {
	vec4 position;    // Camera space
	vec3 diffuse;     // rgb / Idiff
	vec3 specular;    // rgb / Ispec
	vec3 attenuation; // (constant, lineal, quadratic)
	vec3 spotDir;     // Camera space
	float cosCutOff;  // cutOff cosine
	float exponent;
} theLights[4];     // MG_MAX_LIGHTS

uniform struct material_t {
	vec3  diffuse;
	vec3  specular;
	float alpha;
	float shininess;
} theMaterial;

attribute vec3 v_position; // Model space
attribute vec3 v_normal;   // Model space
attribute vec2 v_texCoord;

varying vec4 f_color;
varying vec2 f_texCoord;

//////////////////////////////////////////////////////

vec4 argiInfinitua(int i){

	vec4 pos, normal, r;
	vec3 Idiff, Ispec;

	pos = modelToCameraMatrix * vec4(v_position, 1.0);
	normal = normalize(modelToCameraMatrix * vec4(v_normal,0.0));

	vec4 l = normalize(-1 * theLights[i].position);

	Idiff = theLights[i].diffuse * theMaterial.diffuse;

	r = ( 2*(dot(normal, l)) * normal ) - l;

	Ispec = pow( max(0.0, dot(r, normalize(vec4(-1*pos.xyz, 0.0))  )  ) , theMaterial.shininess) * (theMaterial.specular * theLights[i].specular);

	return ( max(0.0, dot(normal, l)) * vec4(Idiff + Ispec, 0.0)); //  + theLights[0].specular, 0.0) ); //+ Ispec);

}


vec4 argiLokala(int i){

	vec4 pos, normal, r;
	vec3 Idiff, Ispec;

	pos = modelToCameraMatrix * vec4(v_position, 1.0);
	normal = normalize(modelToCameraMatrix * vec4(v_normal,0.0));

	vec4 l = normalize(theLights[i].position - pos);

	Idiff = theLights[i].diffuse * theMaterial.diffuse;

	r = ( 2*(dot(normal, l)) * normal ) - l;

	Ispec = pow( max(0.0, dot(r, normalize(vec4(-1*pos.xyz, 0.0))  )  ) , theMaterial.shininess) * (theMaterial.specular * theLights[i].specular);

	float sc = theLights[i].attenuation[0];
	float sl = theLights[i].attenuation[1];
	float sq = theLights[i].attenuation[2];

	float d = 1/( sc + sl*length(theLights[i].position - pos) + sq * pow(length(theLights[i].position - pos), 2) );

	return d * ( max(0.0, dot(normal,l)) * vec4(Idiff + Ispec, 0.0));

}

vec4 argiSpotlight(int i){

	vec4 pos, normal, r;
	vec3 Idiff, Ispec;

	pos = modelToCameraMatrix * vec4(v_position, 1.0);
	normal = normalize(modelToCameraMatrix * vec4(v_normal,0.0));

	vec4 l = normalize(theLights[i].position - pos);

	Idiff = theLights[i].diffuse * theMaterial.diffuse;

	r = ( 2*(dot(normal, l)) * normal ) - l;

	Ispec = pow( max(0.0, dot(r, normalize(vec4(-1*pos.xyz, 0.0))  )  ) , theMaterial.shininess) * (theMaterial.specular * theLights[i].specular);
	
	// float sc = theLights[i].attenuation[0];
	// float sl = theLights[i].attenuation[1];
	// float sq = theLights[i].attenuation[2];

	// float d = 1/( sc + sl*length(theLights[i].position - pos) + sq * pow(length(theLights[i].position - pos), 2) );
	
	
	float cSpot = max( dot(-1 *l , vec4(theLights[i].spotDir, 0)) , 0);

	if(cSpot < theLights[i].cosCutOff){
		return vec4(0);
	}
	//return vec4(d/255,0,0,0);
	//return cSpot * d * ( max(0.0, dot(normal,normalize(l))) * vec4(Idiff + Ispec, 0.0));
	return pow(cSpot, theLights[i].exponent) * argiLokala(i); // * d;

}



void main() {
	//vec4 pos = modelToCameraMatrix * vec4(v_position, 1.0);
	//vec4 normal = modelToCameraMatrix * vec4(v_normal,0.0);
	
	f_color = vec4(scene_ambient, 1);

	vec4 argia;

	for(int i=0; i<active_lights_n; i++){
		if(theLights[i].position.w == 0){
			argia = argiInfinitua(i);
		}else{
			if(theLights[i].cosCutOff==0){
				argia = argiLokala(i);
			}else{
				argia = argiSpotlight(i);
			}
		}

		f_color += argia;
	}
	
	//f_color = normalize(f_color);

	gl_Position = modelToClipMatrix * vec4(v_position, 1);

	//////////////////////////////////////

	f_texCoord = v_texCoord;
}
